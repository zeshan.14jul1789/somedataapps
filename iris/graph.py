import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

#read in csv
iris_data = sns.load_dataset("iris")
print(iris_data)

#scatter plots for csv to view data "from above"
plot = sns.displot(iris_data, y="sepal_length", x="species", hue="species")
plt.savefig("Sepal_length.png")

plot = sns.displot(iris_data, y="sepal_width", x="species", hue="species")
plt.savefig("Sepal_width.png")

plot = sns.displot(iris_data, y="petal_length", x="species", hue="species")
plt.savefig("petal_length.png")

plot = sns.displot(iris_data, y="petal_width", x="species", hue="species")
plt.savefig("petal_width.png")

#We can see that there are some patterns to observe, now lets see if we can differentiate them. 
