import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import sklearn as skl 

#read in csv
iris_data = pd.DataFrame(sns.load_dataset("iris"))
print(iris_data)

''' scatter plots for csv to view data "from above" '''

plot = sns.displot(iris_data, y="sepal_length", x="species", hue="species")
plt.savefig("Sepal_length.png")

plot = sns.displot(iris_data, y="sepal_width", x="species", hue="species")
plt.savefig("Sepal_width.png")

plot = sns.displot(iris_data, y="petal_length", x="species", hue="species")
plt.savefig("petal_length.png")

plot = sns.displot(iris_data, y="petal_width", x="species", hue="species")
plt.savefig("petal_width.png")

'''We can see that there are some patterns to observe, now lets see how different
   models work in classifying data. '''

#To avoid overfitting we will partition our dataset into a training and testing set. 
shuffled_data = iris_data.sample(frac=1, random_state=2022).reset_index()

training_set = shuffled_data.iloc[10:, :]
test_set = shuffled_data.iloc[:10,:]





